<?php

if (!function_exists('opcache_get_status')) {
  http_response_code(500);
  echo 'OPcache NOT enabled!' . PHP_EOL;
  return;
}

/** @noinspection ForgottenDebugOutputInspection */
var_export(opcache_get_status());
