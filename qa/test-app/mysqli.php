<?php

if (!class_exists(mysqli::class)) {
  http_response_code(500);
  echo 'MySQLi NOT enabled!' . PHP_EOL;
  return;
}

$link = mysqli_connect("mariadb", "develop", "develop", "develop");
if (!$link) {
  http_response_code(500);
  echo 'Failed to connect to database: ' . mysqli_connect_errno() . "/" . mysqli_connect_error() . PHP_EOL;
}

mysqli_close($link);


echo 'Mysqli works fine!' . PHP_EOL;
