<?php

if (!function_exists('gettext')) {
  http_response_code(500);
  echo 'Gettext NOT enabled!' . PHP_EOL;
  return;
}

$result = gettext('Test');
if ($result !== 'Test') {
  http_response_code(500);
  echo 'Gettext does not respond as expected: ' . $result . PHP_EOL;
}

echo 'Gettext works fine!' . PHP_EOL;
