# Docker Xeno-Canto

Base project for specific docker images built and tested specifically for Xeno-Canto

## Trivyignore

The gitlab pipeline checks the security of the build docker images using
[trivy](https://trivy.dev/).
Whenever trivy throws up security issues that stop the pipeline but
can't be fixed yet you can add a CVE number of the issue to the
ignore file.
This process has been centralized in
[lib / trivyignore](https://gitlab.com/naturalis/lib/trivyignore),
you should follow the
[instructions](https://gitlab.com/naturalis/lib/trivyignore#procedure) in the project.

## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).
The pipeline is configured to scan on leaked secrets.

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

 * `pre-commit autoupdate`
 * `pre-commit install`